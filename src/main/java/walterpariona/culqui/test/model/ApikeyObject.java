package walterpariona.culqui.test.model;
import javax.validation.constraints.NotBlank;

public class ApikeyObject {
    @NotBlank
    private String apikey;

    public ApikeyObject(){}

    public ApikeyObject(String apikey) {
        this.apikey = apikey;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }
}
