package walterpariona.culqui.test.model;

import java.text.SimpleDateFormat;
import java.util.Date;

//Class (Model) that represent the response structure of the POST request
public class ResponseObject {
	private String token;
	private String brand;
	private String creation_dt;
	
	public ResponseObject() {}
	
	public ResponseObject(String token, String brand) {
		this.token = token;
		this.brand = brand;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCreation_dt() {
		return creation_dt;
	}

	public void setCreation_dt(String creation_dt) {
		this.creation_dt = creation_dt;
	}

	@Override
	public String toString() {
		return "ResponseObject [token=" + token + ", brand=" + brand + ", creation_dt=" + creation_dt + "]";
	}
	
	//function that registers the created date
	public void registerDate() {
		Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.setCreation_dt(dateFormat.format(date));
	}
	
	
}
