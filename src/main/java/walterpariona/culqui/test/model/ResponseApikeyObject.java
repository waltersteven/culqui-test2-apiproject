package walterpariona.culqui.test.model;

import javax.validation.constraints.NotBlank;

public class ResponseApikeyObject {
	@NotBlank
    private String valid;

    public ResponseApikeyObject(){}

    public ResponseApikeyObject(String valid) {
        this.valid = valid;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }
}
