package walterpariona.culqui.test;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Author: Walter Pariona 
 * */
// using SpringBoot
@SpringBootApplication
// Annotation to use Gson provided as a dependency
@EnableAutoConfiguration(exclude = {org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class})
public class ApiApplication {
	
	public static void main(String[] args) throws IOException {
		// Running Spring application
		SpringApplication.run(ApiApplication.class, args);		
		
		/* NOTE
		 * ENDPOINT: http://localhost:8080/api/tokens 
		 * */
		
		/* Tests has been made with '4444333322221111' as pan number which returns a successful response 
		 * from 'https://lookup.binlist.net/' and let the app continue successfully with the process of returning a response
		 * with the required format: {"token": "tkn_live_4444333322221111-2020-10", "brand": "visa", "creation_dt": "2018-06-20 18:00:00"}.
		
		 * In order to prove the app, a random number where send as pan number (e.g.: 9814333322221111) in the body of the POST request
		 * and the app returns an error since the API from 'https://lookup.binlist.net/' returns a 404.
		*/
	}
}
