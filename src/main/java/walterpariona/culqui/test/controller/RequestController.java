package walterpariona.culqui.test.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import walterpariona.culqui.test.model.ApikeyObject;
import walterpariona.culqui.test.model.ResponseApikeyObject;

/* NOTE
 * ENDPOINT: http://localhost:8080/api/tokens 
 * */

// Class which handles every request needed in the app from external microservices
public class RequestController {
	public static String address = "https://lookup.binlist.net/";
	public static String apikeyAddress = "http://localhost:8081/apikey/validate";
	
	// GET request to web service
	public String getBinList(String bin) throws IOException {	
		System.out.println("getBinList: " + bin);
		String newadrs = address.concat(bin);
		URL url = new URL(newadrs);
		// connection
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
		// defining method
		conn.setRequestMethod("GET");
		
		// headers
		conn.setRequestProperty("Accept-Version", "3");
		
		//reading response
		int respCode = conn.getResponseCode();
		System.out.println("respCode: " + respCode);
	    if (respCode == HttpURLConnection.HTTP_OK) { //success
	    	
	    	System.out.println("-----------------> GET REQUEST TO MICROSERVICE (https://lookup.binlist.net/) WAS SUCCESS");
	    	InputStreamReader inpStr = new InputStreamReader(conn.getInputStream());
	        BufferedReader bufRead = new BufferedReader(inpStr);
	        String inputLine;
	        StringBuffer response = new StringBuffer();
	        while ((inputLine = bufRead .readLine()) != null) {
	            response.append(inputLine);
	        } 
	        //close connection
	        bufRead.close();
	        
	        conn.disconnect();
	        // print result
	        return response.toString();
	    } else { //failed
	        System.out.println("-----------------> GET REQUEST TO MICROSERVICE (https://lookup.binlist.net/) DIDN'T WORK");
	        conn.disconnect();
	        return null;
	    }
	}
	
	public String requestValidation(String headerValue) throws IOException {
		System.out.println("---------------> requestValidation");
		
		RestTemplate restTemplate = new RestTemplate();
		
		ApikeyObject apkobj = new ApikeyObject(headerValue);
		HttpEntity<?> request = new HttpEntity<>(apkobj);
		
		ResponseEntity<ResponseApikeyObject> response = restTemplate.postForEntity( apikeyAddress, request , ResponseApikeyObject.class );	
		
		if (response.getBody().getValid().equalsIgnoreCase("true")) {
			System.out.println("-----------------> GET POST TO APIKEY (http://localhost:8081/api-key/validate) WAS SUCCESS");
			return response.getBody().getValid();
		} else {
			System.out.println("-----------------> GET REQUEST TO VALIDATION SERVICE DIDN'T WORK");
			return response.getBody().getValid();
		}

	}

	// functions that deserializes the JSON in a JSONObjeect and extract the value of specified key (e.g.: schemes)
	public String getValueByKey(String JsonString, String key) {
		JsonObject convertedObject = new Gson().fromJson(JsonString, JsonObject.class);
		JsonElement jsonElm = convertedObject.get(key);
		System.out.println("-----------------> Value of specified key in JSON: " + jsonElm);
        	
		return jsonElm.getAsString();
	}
}
